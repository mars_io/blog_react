import React from 'react';
import { connect } from 'react-redux';
import  { actionCreators } from './store';

import { 
    HeaderWrapper, 
    NavWrapper, 
    Logo, 
    NavMain, 
    NavItem, 
    NavSearch, 
    SearchBtn 
} from './style';

const Header = (props) => {
    return (
        <HeaderWrapper>
            <NavWrapper>
                <Logo>MyBlog</Logo>
                <NavMain>
                    <NavItem href="#" className='left active'>Home</NavItem>
                    <NavItem href="#" className='left'>About</NavItem>
                    <NavItem href="#" className='right'>Login</NavItem>
                    <NavItem href="#" className='right'>Aa</NavItem>
                    <NavSearch 
                        className='left' 
                        placeholder="Search keywords"
                        onFocus={props.handleSearchFocus} 
                        onBlur={props.handleSearchBlur}
                        ></NavSearch>
                    <SearchBtn>Search</SearchBtn>
                </NavMain>
            </NavWrapper>
        </HeaderWrapper>
    );
};
const mapStateToProps = (state) => {
    return {
        focused: state.getIn(['header', 'focused'])
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        handleSearchFocus(){
            dispatch(actionCreators.searchFocus());
        },
        handleSearchBlur() {
            dispatch(actionCreators.searchBlur());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);