import styled from 'styled-components';

const navHeight="56px";
const navWidth = "960px";

export const HeaderWrapper = styled.nav`
    background-color: #fff;
    border: 1px solid #f0f0f0;
    height: ${navHeight};
`;

export const NavWrapper = styled.div`
    min-width: 768px;
    max-width: 1440px;
    margin: 0 auto;
    height: ${navHeight};
`;

export const Logo = styled.a`
    font-weight:bolder;
    font-size: 24px;
    color: #ea6f5a;
    width: 100px;
    display: inline;
    float: left;
    line-height: ${navHeight};
    
`;

export const NavMain = styled.div`
    max-width: ${navWidth};
    height: ${navHeight};
    margin: 0 auto;
`;

export const NavItem = styled.a`
        display: inline;
        line-height: ${navHeight};
        margin-right: 30px;
        font-weight: bolder;
        text-decoration: none;
        &:hover {}
        &.left {
            float: left;
            color: #ea6f5a;
        }
        &.right {
            float: right;
            color: #000;
        }
`;

export const NavSearch = styled.input.attrs({
    type: 'text'
})`
    margin-top: 10px;
    width: 200px;
    border: 1px solid #ccc;
    height: 32px;
    border-radius: 19px;
    outline: none;
    box-sizing: border-box;
    ::placeholder {
        color: #ccc;
        letter-spacing: 1px;
      }
      padding-left: 15px;
`;

export const SearchBtn = styled.button.attrs({
})`
    margin-left : 10px;
    border-radius: 5px;
    height: 30px;
    background-color: #ea6f5a;
    border: none;
    color: #fff;
    &:hover {
        background-color: #e28170;
    }
`;