import React, { Component } from 'react';
import { FooterDiv } from './style';

class Footer extends Component {

    render() {
        return (
            <FooterDiv>This is footer. Just a demo.<br />By mars</FooterDiv>
        );
    }
}

export default Footer;