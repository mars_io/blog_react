import styled from 'styled-components';

export const FooterDiv = styled.div`
    text-align: center;
    font-size: 16px;
    color: #bfbfbc;
`;