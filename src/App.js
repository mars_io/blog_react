import React, {  } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
import Header from './common/header/index';
import Footer from './common/footer/index';
import GlobalStyle from './style';
import store from './store/index';
import Home from './pages/home';
import Detal from './pages/detail';

function App() {
  return (
      <Provider store={store}>
        <GlobalStyle />
        <div className="App">
          <Header />
          <BrowserRouter>
            <Route path="/" exact component={Home} ></Route>
            <Route path="/detail" exact component={Detal} ></Route>
          </BrowserRouter>
          <Footer />
        </div>
      </Provider>
  );
}

export default App;
