import React, { Component } from 'react';
import { RecommendDiv } from '../styled';

class Recommend extends Component {

    render() {
        return (
            <RecommendDiv>
                <ul className="list">
                    <li>
                        <a href="" target="_blank" className="avatar">
                            <img src="/avatar.jpeg"  />
                        </a>
                        <a href="" target="_blank" className="name">
                        行距版君
                        </a> 
                        <p>
                            写了495.6k字 · 53.8k喜欢
                        </p>
                    </li>

                    <li>
                        <a href="" target="_blank" className="avatar">
                            <img src="/avatar.jpeg"  />
                        </a>
                        <a href="" target="_blank" className="name">
                        行距版君
                        </a> 
                        <p>
                            写了495.6k字 · 53.8k喜欢
                        </p>
                    </li>

                    <li>
                        <a href="" target="_blank" className="avatar">
                            <img src="/avatar.jpeg"  />
                        </a>
                        <a href="" target="_blank" className="name">
                        行距版君
                        </a> 
                        <p>
                            写了495.6k字 · 53.8k喜欢
                        </p>
                    </li>
                </ul>
            </RecommendDiv>
                );
            }
        }
        
export default Recommend;