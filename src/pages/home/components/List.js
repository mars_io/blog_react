import React, { Component } from 'react';
import { ListDiv } from '../styled';

class List extends Component {

    render() {
        return (
            <ListDiv>
                <ul>
                    <li id="note-40911027" data-note-id="40911027" className="have-img">
                        <a>
                        <img src="/list.jpeg" />
                        </a>
                        <div className="content">
                            <a className="title" target="_blank" href="">
                                坚持每天做好6件事情
                                </a>
                            <p className="abstract">
                            你几岁啦？ 之所以想到这个问题是因为我发现随着年龄的增长，我们会觉得时间过的越来越快。小的时候，一天到晚四处玩闹仍觉得时间过得特别慢，现在就是，...
                            </p>
                        </div>
                 </li>
                 <li id="note-40911027" data-note-id="40911027" className="have-img">
                        <a>
                        <img src="/list.jpeg" />
                        </a>
                        <div className="content">
                            <a className="title" target="_blank" href="">
                                坚持每天做好6件事情
                                </a>
                            <p className="abstract">
                            你几岁啦？ 之所以想到这个问题是因为我发现随着年龄的增长，我们会觉得时间过的越来越快。小的时候，一天到晚四处玩闹仍觉得时间过得特别慢，现在就是，...
                            </p>
                        </div>
                 </li>
                 <li id="note-40911027" data-note-id="40911027" className="have-img">
                        <a>
                        <img src="/list.jpeg" />
                        </a>
                        <div className="content">
                            <a className="title" target="_blank" href="">
                                坚持每天做好6件事情
                                </a>
                            <p className="abstract">
                            你几岁啦？ 之所以想到这个问题是因为我发现随着年龄的增长，我们会觉得时间过的越来越快。小的时候，一天到晚四处玩闹仍觉得时间过得特别慢，现在就是，...
                            </p>
                        </div>
                 </li>
                </ul>
            </ListDiv>
            
        );
    }
}

export default List;