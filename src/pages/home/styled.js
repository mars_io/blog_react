import styled from 'styled-components';

export const HomeWrapper = styled.div`
    width: 960px;
    margin: 20px auto;
    overflow: hidden;
`;

export const HomeLeft = styled.div`
    width: 625px;
    float:left;
    .banner-img {
        width: 625px;
        height: 270px;
    }

`;

export const HomeRight = styled.div`
    width: 240px;
    float: right;
`;

export const ListDiv = styled.div`

    >ul {
        margin-top: 30px;
    }

    > ul li{
        overflow: hidden;

        margin-bottom: 50px;

        margin-top: 50px;
        border-bottom: 1px solid #f9f3f3;

        > a {
            float: right;

            > img {
                width : 148px;
                height: 98px;
            }
        }
        .content {
            
            > a {
                margin: -7px 0 4px;
                display: inherit;
                font-size: 22px;
                font-weight: 700;
                line-height: 1.5;
                text-decoration: none;
                color: #333;
            }

            > p {
                margin: 0 0 8px;
                font-size: 16px;
                line-height: 24px;
                color: #999;
            }
        }
    }
`;

export const RecommendDiv = styled.div`
    > ul li {
        float: left;
        margin-bottom: 30px;
    }

    > ul li a{
        float: left;
        color: #333;
        text-decoration: none;
        > img {
            width: 80%;
            height: 100%;
            border: 1px solid #ddd;
            border-radius: 50%;
            margin-top: 10px;
        }
    }

    .name {
        padding-top: 5px;
        margin-right: 60px;
        margin-top: 15px;
        font-size: 20px;
        display: block;
    }

    > ul li p {
        display: block;
        float: left;
        font-size: 12px;
        margin-top: 28px;
        color: #969696;
    }

`;