import React, { Component } from 'react';
import { HomeWrapper, HomeLeft, HomeRight } from './styled';
import Recommend from './components/Recommend';
import List from './components/List';

class Home extends Component {

    render() {
        return (
            <HomeWrapper>
                <HomeLeft>
                    <img src="/banner.jpg" className='banner-img' />
                    <List />
                </HomeLeft>

                <HomeRight>
                    <Recommend />
                </HomeRight>

            </HomeWrapper>
        );
    }

}

export default Home;