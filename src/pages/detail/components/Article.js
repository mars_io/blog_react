import React, { Component } from 'react';
import { ArticleDiv } from '../styled';

class Article extends Component {

    render() {
        return (

            <ArticleDiv>
                <div className="post">
                <div className="article">
                    <h1 className="title">那些年网易云直戳泪点的评论</h1>

                    <div className="content">
                    @故事里的少年们生活在一个不算遥远的过去，
                    一个青瓦小巷，墙皮斑驳的小城，那里阳光温暖却不浓烈。
                    校门口的街边总是弥漫着煎炸小食油腻腻的香气，
                    男生踩在发出各种声响的老旧自行车上高声谈笑，
                    女生永远穿着宽大的校服，在婆娑的树影里谈笑，
                    风吹过身旁，才能看见他们瘦小纤细的轮廓……

                    @故事里的少年们生活在一个不算遥远的过去，
                    一个青瓦小巷，墙皮斑驳的小城，那里阳光温暖却不浓烈。
                    校门口的街边总是弥漫着煎炸小食油腻腻的香气，
                    男生踩在发出各种声响的老旧自行车上高声谈笑，
                    女生永远穿着宽大的校服，在婆娑的树影里谈笑，
                    风吹过身旁，才能看见他们瘦小纤细的轮廓……

                    @故事里的少年们生活在一个不算遥远的过去，
                    一个青瓦小巷，墙皮斑驳的小城，那里阳光温暖却不浓烈。
                    校门口的街边总是弥漫着煎炸小食油腻腻的香气，
                    男生踩在发出各种声响的老旧自行车上高声谈笑，
                    女生永远穿着宽大的校服，在婆娑的树影里谈笑，
                    风吹过身旁，才能看见他们瘦小纤细的轮廓……
                    </div>
                </div>
                </div>
            </ArticleDiv>
        );
    }
}

export default Article;