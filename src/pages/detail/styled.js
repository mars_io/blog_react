import styled from 'styled-components';

export const ArticleDiv = styled.div`
    width: 960px;
    margin: 0 auto;

    .title {
        word-break: break-word!important;
        word-break: break-all;
        margin: 40px 0 0;
        font-size: 34px;
        font-weight: 700;
        line-height: 1.3;
    }

    .content {
        line-height: 50px;
        margin-top: 40px;
    }
`;